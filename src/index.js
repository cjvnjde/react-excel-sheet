import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Spreadsheet from './components/Spreadsheet';

ReactDOM.render(
  React.createElement(Spreadsheet, null, null),
  document.getElementById('root'),
);
