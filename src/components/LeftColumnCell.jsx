import React from 'react';
import PropTypes from 'prop-types';

const LeftColumnCell = (props) => {
  const {
    style,
    rowIndex,
    columnIndex,
    parent,
    cache,
  } = props;
  const mouse = {
    startY: 0, endY: 0,
  };
  return (
    <div style={style}>
      <div style={{
        display: 'flex',
        alignContent: 'space-between',
        flexDirection: 'column',
        height: '100%',
      }}
      >
        <div style={{
          textAlign: 'center',
          height: '100%',
        }}
        >
          {rowIndex}
        </div>
        <div
          style={{
            cursor: 'row-resize',
            backgroundColor: 'grey',
            height: '4px',
            width: '100%',
          }}
          draggable
          onDragStart={(e) => {
            mouse.startY = e.screenY;
            e.dataTransfer.setData('text/plain', 'draggable');
          }
        }
          onDragEnd={(e) => {
            mouse.endY = e.screenY;
            if (Math.abs(mouse.startY - mouse.endY) >= cache.defaultHeight) {
              cache.set(rowIndex,
                columnIndex,
                cache.defaultHeight,
                style.height - mouse.startY + mouse.endY);
              parent.recomputeGridSize({ columnIndex, rowIndex });
            }
          }}
        />
      </div>
    </div>
  );
};

LeftColumnCell.propTypes = {
  style: PropTypes.object.isRequired,
  rowIndex: PropTypes.number.isRequired,
  columnIndex: PropTypes.number.isRequired,
  parent: PropTypes.object.isRequired,
  cache: PropTypes.object.isRequired,
};


export default LeftColumnCell;
