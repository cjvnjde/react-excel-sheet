import React, { Component } from 'react';
// components
import Workbook from './Workbook';

class Spreadsheet extends Component {
  state = {
    height: 100,
    width: 100,
  }

  componentDidMount() {
    this.updateDimensions();
    const updateDimensions = this.debounce(this.updateDimensions, 250);
    window.addEventListener('resize', updateDimensions);
  }

  debounce = (func, wait, immediate) => {
    let timeout;
    return (...args) => {
      const later = () => {
        timeout = null;
        if (!immediate) func.apply(this, args);
      };
      const callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) func.apply(this, args);
    };
  }

  updateDimensions = () => {
    this.setState(() => ({
      width: window.innerWidth,
      height: window.innerHeight,
    }));
  }

  render() {
    const { height, width } = this.state;
    return (
      <Workbook
        height={height}
        width={width}
        cellWidth={90}
        cellHeight={30}
        columnCount={100}
        rowCount={100}
      />

    );
  }
}

export default Spreadsheet;
