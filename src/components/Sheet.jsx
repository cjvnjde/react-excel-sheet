import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { CellMeasurer, CellMeasurerCache } from 'react-virtualized';
// helpers
import { coordToString } from '../helpers';
// components
import ExcelGrid from './ExcelGrid';
import CellBody from './CellBody';
import LeftColumnCell from './LeftColumnCell'
import TopRowCell from './TopRowCell'


class Sheet extends Component {
  static propTypes = {
    onChange: PropTypes.func.isRequired,
    inputs: PropTypes.instanceOf(Map).isRequired,
    calculated: PropTypes.instanceOf(Map).isRequired,
    height: PropTypes.number.isRequired,
    width: PropTypes.number.isRequired,
    cellWidth: PropTypes.number.isRequired,
    cellHeight: PropTypes.number.isRequired,
    columnCount: PropTypes.number.isRequired,
    rowCount: PropTypes.number.isRequired,
  }

  constructor(props) {
    super(props);
    this.cache = new CellMeasurerCache({
      defaultWidth: props.cellWidth,
      defaultHeight: props.cellHeight,
      minWidth: props.cellWidth,
      minHeight: props.cellHeight,
    });
  }

  cellRendererBody = ({
    columnIndex,
    isScrolling,
    key,
    rowIndex,
    style,
    parent,
  }) => {
    const coord = coordToString({ column: columnIndex, row: rowIndex });
    const {
      onChange,
      inputs,
      calculated,
    } = this.props;

    const style1 = Object.assign({}, style);

    if (rowIndex % 2 === 0) {
      style1.backgroundColor = 'white';
    } else {
      style1.backgroundColor = '#f0f0f0';
    }

    style1.cursor = 'cell';
    let inputValue = '';
    let calculatedValue = '';

    if (inputs.has(coord)) {
      inputValue = inputs.get(coord);
    }

    if (calculated.has(coord)) {
      calculatedValue = calculated.get(coord);
    } else if (inputValue !== '') {
      style1.backgroundColor = 'rgba(255, 0, 0, 0.1)';
      calculatedValue = inputValue;
    }

    let content = isScrolling
      ? <div key={key} style={style1}>...</div>
      : (
        <CellBody
          key={key}
          style={style1}
          onChange={(e) => {
            onChange(coord, e);
            parent.recomputeGridSize({ columnIndex, rowIndex });
          }}
          inputValue={inputValue}
          calculatedValue={calculatedValue}
        />
      );

    if (rowIndex === 0) {
      content = (
        <TopRowCell 
        key={key}
        style={style1}
        rowIndex={rowIndex}
        columnIndex={columnIndex}
        parent={parent}
        cache={this.cache}
        />
      );
    }


    if (columnIndex === 0) {
      content = rowIndex === 0
        ? <div key={key} style={style1} />
        : <LeftColumnCell 
        key={key}
        style={style1}
        rowIndex={rowIndex}
        columnIndex={columnIndex}
        parent={parent}
        cache={this.cache}
        />;
    }

    return (
      <CellMeasurer
        cache={this.cache}
        columnIndex={columnIndex}
        key={key}
        parent={parent}
        rowIndex={rowIndex}
      >
        {content}
      </CellMeasurer>);
  }

  render() {
    const {
      height,
      width,
      rowCount,
      columnCount,
    } = this.props;
    return (
      <ExcelGrid
        cellRendererTop={this.cellRendererTop}
        height={height}
        width={width}
        deferredMeasurementCache={this.cache}
        cellWidth={this.cache.columnWidth}
        columnCount={columnCount}
        cellHeight={this.cache.rowHeight}
        rowCount={rowCount}
        cellRendererLeft={this.cellRendererLeft}
        cellRendererBody={this.cellRendererBody}
      />
    );
  }
}

export default Sheet;
