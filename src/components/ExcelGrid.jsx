

import React from 'react';
import { MultiGrid } from 'react-virtualized';
import PropTypes from 'prop-types';

const ExcelGrid = (props) => {
  const {
    width,
    cellWidth,
    columnCount,
    cellHeight,
    rowCount,
    height,
    cellRendererBody,
    deferredMeasurementCache,
  } = props;

  return (
    <MultiGrid
      fixedColumnCount={1}
      fixedRowCount={1}
      cellRenderer={cellRendererBody}
      columnCount={columnCount}
      columnWidth={cellWidth}
      height={height}
      rowCount={rowCount}
      rowHeight={cellHeight}
      width={width}
      deferredMeasurementCache={deferredMeasurementCache}
    />
  );
};

ExcelGrid.propTypes = {
  width: PropTypes.number.isRequired,
  cellWidth: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.func,
  ]).isRequired,
  columnCount: PropTypes.number.isRequired,
  cellHeight: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.func,
  ]).isRequired,
  rowCount: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  cellRendererBody: PropTypes.func.isRequired,
  deferredMeasurementCache: PropTypes.object.isRequired,
};

export default ExcelGrid;
