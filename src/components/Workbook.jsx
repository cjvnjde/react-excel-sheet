import React, { Component } from 'react';
import { Parser } from 'hot-formula-parser';
import PropTypes from 'prop-types';
// components
import Sheet from './Sheet';
// helpers
import { coordToString } from '../helpers';

class Workbook extends Component {
  static propTypes = {
    height: PropTypes.number.isRequired,
    width: PropTypes.number.isRequired,
    cellWidth: PropTypes.number.isRequired,
    cellHeight: PropTypes.number.isRequired,
    columnCount: PropTypes.number.isRequired,
    rowCount: PropTypes.number.isRequired,
    // if you need to set the inputs outside
    inputs: PropTypes.instanceOf(Map),
  }

  static defaultProps = {
    inputs: new Map(),
  }

  constructor(props) {
    super(props);
    this.parser = new Parser();
    // key = coordToString like A1, FB5
    this.inputs = props.inputs;
    this.calculated = new Map();

    this.parser.on('callCellValue', (cellCoord, done) => {
      const coord = {
        column: cellCoord.column.index + 1,
        row: cellCoord.row.index + 1,
      };

      const key = coordToString(coord);

      if (this.calculated.has(key)) {
        done(this.calculated.get(key));
      } else {
        done(0);
      }
    });

    this.parser.on('callRangeValue', (startCellCoord, endCellCoord, done) => {
      const fragment = [];
      for (let row = startCellCoord.row.index + 1; row <= endCellCoord.row.index + 1; row += 1) {
        const colFragment = [];
        for (let column = startCellCoord.column.index + 1;
          column <= endCellCoord.column.index + 1;
          column += 1) {
          const coord = coordToString({ column, row });
          let data = 0;
          if (this.calculated.has(coord)) {
            data = this.calculated.get(coord);
          }
          colFragment.push(data);
        }
        fragment.push(colFragment);
      }
      if (fragment) {
        done(fragment);
      }
    });
  }

  onChange = (coord, e) => {
    this.inputs.set(coord, e.target.value.replace(/\n/, ''));
    this.dataParser();
  }

  dataParser = () => {
    const { parser } = this;
    const dependencies = new Map();
    this.calculated.clear();
    const cellCheck = /([A-Z]{1,5})([0-9]{1,5})/g;
    let toCalculate = 0;
    this.inputs.forEach((value, key) => {
      if (/^=/.test(value) || /^\d+$/.test(value)) {
        toCalculate += 1;
        const cells = value.match(cellCheck);
        if (dependencies.has(key)) {
          dependencies.delete(key);
          cells && dependencies.set(key, cells);
        } else {
          cells && dependencies.set(key, cells);
        }
      }
    });
    const cicleLimit = this.inputs.size * this.inputs.size * this.inputs.size;
    let cicleCounter = 0;

    while (toCalculate !== this.calculated.size) {
      cicleCounter += 1;
      this.inputs.forEach((value, key) => {
        if (/^=/.test(value) || /^\d+$/.test(value)) {
          const dep = dependencies.get(key);
          if (!dep) {
            const { result } = parser.parse(
              value.replace('=', ''),
            );
            this.calculated.set(key, `${result}`);
          } else {
            this.calculated.delete(key);
            let needCalc = false;
            dep.forEach((depValue) => {
              if (!needCalc && !this.calculated.has(depValue)) {
                needCalc = true;
              }
            });

            if (!needCalc) {
              const { result } = parser.parse(
                value.substring(1, value.length),
              );
              this.calculated.set(key, `${result}`);
            }
          }
        }
      });
      if (cicleCounter >= cicleLimit) {
        break;
      }
    }
  }

  render() {
    const {
      height,
      width,
      cellWidth,
      cellHeight,
      columnCount,
      rowCount,
    } = this.props;
    return (
      <Sheet
        onChange={this.onChange}
        inputs={this.inputs}
        calculated={this.calculated}
        height={height}
        width={width}
        cellWidth={cellWidth}
        cellHeight={cellHeight}
        columnCount={columnCount}
        rowCount={rowCount}
      />
    );
  }
}

export default Workbook;
