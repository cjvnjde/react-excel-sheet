/*  eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

class CellBody extends Component {
  static propTypes = {
    style: PropTypes.shape({
      height: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
      ]).isRequired,
      width: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
      ]).isRequired,
      left: PropTypes.number.isRequired,
      position: PropTypes.string.isRequired,
      top: PropTypes.number.isRequired,
    }).isRequired,
    onChange: PropTypes.func.isRequired,
    inputValue: PropTypes.string,
    calculatedValue: PropTypes.string,
  }

  static defaultProps = {
    inputValue: '',
    calculatedValue: '',
  }

  constructor(props) {
    super(props);
    this.textInput = React.createRef();
  }

  state = {
    isChanging: false,
  }

  componentDidUpdate() {
    if (this.textInput.current) this.textInput.current.focus();
  }

  render() {
    const { isChanging } = this.state;
    const {
      style, onChange, inputValue, calculatedValue,
    } = this.props;
    if (isChanging) {
      return (
        <div style={{
          ...style,
          border: 'solid 1px grey',
        }}
        >
          <textarea
            style={{
              whiteSpace: 'nowrap',
              overflow: 'hidden',
              width: style.width - 3,
              height: style.height - 3,
            }}
            ref={this.textInput}
            onChange={onChange}
            value={inputValue}
            onBlur={
          () => {
            this.setState((state, props) => ({ isChanging: !state.isChanging }));
          }
        }
          />
        </div>
      );
    }
    return (
      <div
        style={{ ...style, border: 'solid 1px grey' }}
        onClick={
        () => {
          this.setState((state, props) => ({ isChanging: !state.isChanging }));
        }
      }
      >
        {calculatedValue}
      </div>
    );
  }
}

export default CellBody;
