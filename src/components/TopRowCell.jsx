import React from 'react';
import PropTypes from 'prop-types';
// helpers
import { columnToLetter } from '../helpers';

const TopRowCell = (props) => {
  const {
    style,
    rowIndex,
    columnIndex,
    parent,
    cache,
  } = props;
  const mouse = {
    startX: 0, endX: 0,
  };
  return (
    <div style={style}>
      <div style={
              {
                display: 'flex',
                flexDirection: 'row',
                height: '100%',
              }
            }
      >
        <div style={{
          textAlign: 'center',
          width: '100%',
        }}
        >
          {columnToLetter(columnIndex)}
        </div>
        <div
          style={{
            cursor: 'col-resize',
            backgroundColor: 'grey',
            width: '4px',
            height: '100%',
          }}
          draggable
          onDragStart={(e) => {
            mouse.startX = e.screenX;
            e.dataTransfer.setData('text/plain', 'draggable');
          }
              }
          onDragEnd={(e) => {
            mouse.endX = e.screenX;
            cache.set(rowIndex,
              columnIndex,
              style.width - mouse.startX + mouse.endX,
              cache.defaultHeight);
            parent.recomputeGridSize({ columnIndex, rowIndex });
          }}
        />
      </div>
    </div>
  );
};

TopRowCell.propTypes = {
  style: PropTypes.object.isRequired,
  rowIndex: PropTypes.number.isRequired,
  columnIndex: PropTypes.number.isRequired,
  parent: PropTypes.object.isRequired,
  cache: PropTypes.object.isRequired,
};

export default TopRowCell;
