[![NPM version](https://img.shields.io/npm/v/react-virtualized.svg?style=flat)](https://www.npmjs.com/package/react-excel-sheet)

[demo page](https://cjvnjde.gitlab.io/react-excel-sheet/)

![gif](https://api.monosnap.com/rpc/file/download?id=ebD264mmWkzNjBJ8l5HbJ1tsiRxWvk)

Getting started
---------------
Install `react-excel-sheet` using npm.
```shell
npm install react-excel-sheet --save
```

```js
import Workbook from 'react-excel-sheet'

...

<Workbook
        height={500}
        width={500}
        cellWidth={90}
        cellHeight={30}
        columnCount={100}
        rowCount={100}
      />

```

### TODO:
- [ ] Keyboard navigation
- [x] Resizable cell
- [ ] Infinite scroll
- [ ] Selection
- [x] Numeric input without equal sign