const path = require('path');

module.exports = {
  resolveLoader: {
    modules: [path.resolve(__dirname), 'node_modules'],
  },
  entry: {
    index: './index.js',
    package: './package.json',
  },
  mode: 'production',
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: '[name].js',
    libraryTarget: 'commonjs2',
  },
  resolve: {
    extensions: ['.js', '.jsx'],
  },
  module: {
    rules: [
      {
        test: /\.jsx$/,
        include: path.resolve(__dirname, 'src'),
        exclude: /(node_modules)/,
        loader: 'babel-loader',
        options: {
          presets: [
            '@babel/preset-env',
            '@babel/preset-react',
          ],
          plugins: [
            '@babel/plugin-proposal-class-properties',
            ['transform-react-remove-prop-types', {
              removeImport: true,
              ignoreFilenames: ['node_modules'],
            }],
          ],
        },
      },
      {
        test: /package.json$/,
        loader: 'package-loader',
      },
    ],
  },

};
