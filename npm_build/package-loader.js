const fs = require('fs');

module.exports = function (source) {
  const pkg = JSON.parse(fs.readFileSync('../package.json'));
  const merged = Object.assign({}, JSON.parse(source), {
    name: pkg.name,
    version: pkg.version,
    description: pkg.description,
    license: pkg.license,
    homepage: pkg.homepage,
    author: pkg.author,
    repository: pkg.repository,
  });
  const mergedJson = JSON.stringify(merged);
  this.emitFile('package.json', mergedJson);
  return mergedJson;
};
